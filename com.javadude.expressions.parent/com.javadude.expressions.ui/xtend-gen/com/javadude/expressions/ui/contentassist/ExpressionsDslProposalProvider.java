/**
 * generated by Xtext 2.14.0
 */
package com.javadude.expressions.ui.contentassist;

import com.javadude.expressions.ui.contentassist.AbstractExpressionsDslProposalProvider;

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
@SuppressWarnings("all")
public class ExpressionsDslProposalProvider extends AbstractExpressionsDslProposalProvider {
}
