import com.javadude.expressions.ExpressionsDslStandaloneSetup
import com.javadude.expressions.expressionsDsl.Addition
import com.javadude.expressions.expressionsDsl.Assignment
import com.javadude.expressions.expressionsDsl.Expression
import com.javadude.expressions.expressionsDsl.Literal
import com.javadude.expressions.expressionsDsl.Model
import com.javadude.expressions.expressionsDsl.Multiplication
import com.javadude.expressions.expressionsDsl.Statement
import com.javadude.expressions.expressionsDsl.VariableRef
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.diagnostics.Severity
import org.eclipse.xtext.util.CancelIndicator
import org.eclipse.xtext.validation.CheckMode
import org.eclipse.xtext.validation.IResourceValidator

class Test2 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val injector = ExpressionsDslStandaloneSetup().createInjectorAndDoEMFRegistration()
            val rs = injector.getInstance(ResourceSet::class.java)
            val resource = rs.getResource(URI.createURI("test1/test1.expressions"), true)
            resource.load(null)
            val model = resource.contents[0] as Model

            val validator = injector.getInstance(IResourceValidator::class.java)
            val issues = validator.validate(resource, CheckMode.ALL, CancelIndicator.NullImpl)
            var isEvil = false
            for (issue in issues) {
                println("${issue.severity.name}: ${issue.lineNumber}:${issue.column}:  ${issue.message}")
                if (issue.severity == Severity.ERROR) {
                    isEvil = true
                }
            }

            if (!isEvil) {
                var result = 0
                model.statements.forEach {
                    result = evaluate(it)
                }
                println(result)
            } else {
                println("Execution aborted. So there.")
            }

        }

        private val variables = mutableMapOf<String, Int>()

        private fun evaluate(x : Statement) : Int {
            return when (x) {
                is Assignment -> evaluate(x)
                is Expression -> evaluate(x)
                else -> throw IllegalArgumentException("Unknown type ${x::class}")
            }
        }
        private fun evaluate(x : Expression) : Int {
            return when (x) {
                is Addition -> evaluate(x)
                is Multiplication -> evaluate(x)
                is VariableRef -> evaluate(x)
                is Literal -> evaluate(x)
                else -> throw IllegalArgumentException("Unknown type ${x::class}")
            }
        }
        private fun evaluate(x : Addition) : Int {
           return when (x.op) {
               "+" -> evaluate(x.op1) + evaluate(x.op2)
               else -> evaluate(x.op1) - evaluate(x.op2)
           }
        }
        private fun evaluate(x : Multiplication) : Int {
           return when (x.op) {
               "*" -> evaluate(x.op1) * evaluate(x.op2)
               else -> evaluate(x.op1) / evaluate(x.op2)
           }
        }
        private fun evaluate(x : VariableRef) : Int {
            return variables[x.variable.name] ?: throw IllegalArgumentException("variable ${x.variable.name} has no value")
        }

        private fun evaluate(x : Literal) = x.value

        private fun evaluate(x : Assignment) =
            evaluate(x.value).also {
                variables[x.variable.name] = it
            }
    }

}
