package com.javadude.expressions.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import com.javadude.expressions.services.ExpressionsDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalExpressionsDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'+'", "'-'", "'*'", "'/'", "'var'", "'='", "'('", "')'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=6;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int RULE_INT=5;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalExpressionsDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalExpressionsDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalExpressionsDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalExpressionsDsl.g"; }


    	private ExpressionsDslGrammarAccess grammarAccess;

    	public void setGrammarAccess(ExpressionsDslGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleModel"
    // InternalExpressionsDsl.g:53:1: entryRuleModel : ruleModel EOF ;
    public final void entryRuleModel() throws RecognitionException {
        try {
            // InternalExpressionsDsl.g:54:1: ( ruleModel EOF )
            // InternalExpressionsDsl.g:55:1: ruleModel EOF
            {
             before(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            ruleModel();

            state._fsp--;

             after(grammarAccess.getModelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalExpressionsDsl.g:62:1: ruleModel : ( ( rule__Model__Group__0 ) ) ;
    public final void ruleModel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:66:2: ( ( ( rule__Model__Group__0 ) ) )
            // InternalExpressionsDsl.g:67:2: ( ( rule__Model__Group__0 ) )
            {
            // InternalExpressionsDsl.g:67:2: ( ( rule__Model__Group__0 ) )
            // InternalExpressionsDsl.g:68:3: ( rule__Model__Group__0 )
            {
             before(grammarAccess.getModelAccess().getGroup()); 
            // InternalExpressionsDsl.g:69:3: ( rule__Model__Group__0 )
            // InternalExpressionsDsl.g:69:4: rule__Model__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Model__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleStatement"
    // InternalExpressionsDsl.g:78:1: entryRuleStatement : ruleStatement EOF ;
    public final void entryRuleStatement() throws RecognitionException {
        try {
            // InternalExpressionsDsl.g:79:1: ( ruleStatement EOF )
            // InternalExpressionsDsl.g:80:1: ruleStatement EOF
            {
             before(grammarAccess.getStatementRule()); 
            pushFollow(FOLLOW_1);
            ruleStatement();

            state._fsp--;

             after(grammarAccess.getStatementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStatement"


    // $ANTLR start "ruleStatement"
    // InternalExpressionsDsl.g:87:1: ruleStatement : ( ( rule__Statement__Alternatives ) ) ;
    public final void ruleStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:91:2: ( ( ( rule__Statement__Alternatives ) ) )
            // InternalExpressionsDsl.g:92:2: ( ( rule__Statement__Alternatives ) )
            {
            // InternalExpressionsDsl.g:92:2: ( ( rule__Statement__Alternatives ) )
            // InternalExpressionsDsl.g:93:3: ( rule__Statement__Alternatives )
            {
             before(grammarAccess.getStatementAccess().getAlternatives()); 
            // InternalExpressionsDsl.g:94:3: ( rule__Statement__Alternatives )
            // InternalExpressionsDsl.g:94:4: rule__Statement__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Statement__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getStatementAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStatement"


    // $ANTLR start "entryRuleVariable"
    // InternalExpressionsDsl.g:103:1: entryRuleVariable : ruleVariable EOF ;
    public final void entryRuleVariable() throws RecognitionException {
        try {
            // InternalExpressionsDsl.g:104:1: ( ruleVariable EOF )
            // InternalExpressionsDsl.g:105:1: ruleVariable EOF
            {
             before(grammarAccess.getVariableRule()); 
            pushFollow(FOLLOW_1);
            ruleVariable();

            state._fsp--;

             after(grammarAccess.getVariableRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalExpressionsDsl.g:112:1: ruleVariable : ( ( rule__Variable__Group__0 ) ) ;
    public final void ruleVariable() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:116:2: ( ( ( rule__Variable__Group__0 ) ) )
            // InternalExpressionsDsl.g:117:2: ( ( rule__Variable__Group__0 ) )
            {
            // InternalExpressionsDsl.g:117:2: ( ( rule__Variable__Group__0 ) )
            // InternalExpressionsDsl.g:118:3: ( rule__Variable__Group__0 )
            {
             before(grammarAccess.getVariableAccess().getGroup()); 
            // InternalExpressionsDsl.g:119:3: ( rule__Variable__Group__0 )
            // InternalExpressionsDsl.g:119:4: rule__Variable__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Variable__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVariableAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleAssignment"
    // InternalExpressionsDsl.g:128:1: entryRuleAssignment : ruleAssignment EOF ;
    public final void entryRuleAssignment() throws RecognitionException {
        try {
            // InternalExpressionsDsl.g:129:1: ( ruleAssignment EOF )
            // InternalExpressionsDsl.g:130:1: ruleAssignment EOF
            {
             before(grammarAccess.getAssignmentRule()); 
            pushFollow(FOLLOW_1);
            ruleAssignment();

            state._fsp--;

             after(grammarAccess.getAssignmentRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAssignment"


    // $ANTLR start "ruleAssignment"
    // InternalExpressionsDsl.g:137:1: ruleAssignment : ( ( rule__Assignment__Group__0 ) ) ;
    public final void ruleAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:141:2: ( ( ( rule__Assignment__Group__0 ) ) )
            // InternalExpressionsDsl.g:142:2: ( ( rule__Assignment__Group__0 ) )
            {
            // InternalExpressionsDsl.g:142:2: ( ( rule__Assignment__Group__0 ) )
            // InternalExpressionsDsl.g:143:3: ( rule__Assignment__Group__0 )
            {
             before(grammarAccess.getAssignmentAccess().getGroup()); 
            // InternalExpressionsDsl.g:144:3: ( rule__Assignment__Group__0 )
            // InternalExpressionsDsl.g:144:4: rule__Assignment__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Assignment__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAssignmentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAssignment"


    // $ANTLR start "entryRuleAddition"
    // InternalExpressionsDsl.g:153:1: entryRuleAddition : ruleAddition EOF ;
    public final void entryRuleAddition() throws RecognitionException {
        try {
            // InternalExpressionsDsl.g:154:1: ( ruleAddition EOF )
            // InternalExpressionsDsl.g:155:1: ruleAddition EOF
            {
             before(grammarAccess.getAdditionRule()); 
            pushFollow(FOLLOW_1);
            ruleAddition();

            state._fsp--;

             after(grammarAccess.getAdditionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAddition"


    // $ANTLR start "ruleAddition"
    // InternalExpressionsDsl.g:162:1: ruleAddition : ( ( rule__Addition__Group__0 ) ) ;
    public final void ruleAddition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:166:2: ( ( ( rule__Addition__Group__0 ) ) )
            // InternalExpressionsDsl.g:167:2: ( ( rule__Addition__Group__0 ) )
            {
            // InternalExpressionsDsl.g:167:2: ( ( rule__Addition__Group__0 ) )
            // InternalExpressionsDsl.g:168:3: ( rule__Addition__Group__0 )
            {
             before(grammarAccess.getAdditionAccess().getGroup()); 
            // InternalExpressionsDsl.g:169:3: ( rule__Addition__Group__0 )
            // InternalExpressionsDsl.g:169:4: rule__Addition__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Addition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAdditionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAddition"


    // $ANTLR start "entryRuleMultiplication"
    // InternalExpressionsDsl.g:178:1: entryRuleMultiplication : ruleMultiplication EOF ;
    public final void entryRuleMultiplication() throws RecognitionException {
        try {
            // InternalExpressionsDsl.g:179:1: ( ruleMultiplication EOF )
            // InternalExpressionsDsl.g:180:1: ruleMultiplication EOF
            {
             before(grammarAccess.getMultiplicationRule()); 
            pushFollow(FOLLOW_1);
            ruleMultiplication();

            state._fsp--;

             after(grammarAccess.getMultiplicationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMultiplication"


    // $ANTLR start "ruleMultiplication"
    // InternalExpressionsDsl.g:187:1: ruleMultiplication : ( ( rule__Multiplication__Group__0 ) ) ;
    public final void ruleMultiplication() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:191:2: ( ( ( rule__Multiplication__Group__0 ) ) )
            // InternalExpressionsDsl.g:192:2: ( ( rule__Multiplication__Group__0 ) )
            {
            // InternalExpressionsDsl.g:192:2: ( ( rule__Multiplication__Group__0 ) )
            // InternalExpressionsDsl.g:193:3: ( rule__Multiplication__Group__0 )
            {
             before(grammarAccess.getMultiplicationAccess().getGroup()); 
            // InternalExpressionsDsl.g:194:3: ( rule__Multiplication__Group__0 )
            // InternalExpressionsDsl.g:194:4: rule__Multiplication__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Multiplication__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMultiplicationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMultiplication"


    // $ANTLR start "entryRulePrimary"
    // InternalExpressionsDsl.g:203:1: entryRulePrimary : rulePrimary EOF ;
    public final void entryRulePrimary() throws RecognitionException {
        try {
            // InternalExpressionsDsl.g:204:1: ( rulePrimary EOF )
            // InternalExpressionsDsl.g:205:1: rulePrimary EOF
            {
             before(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalExpressionsDsl.g:212:1: rulePrimary : ( ( rule__Primary__Alternatives ) ) ;
    public final void rulePrimary() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:216:2: ( ( ( rule__Primary__Alternatives ) ) )
            // InternalExpressionsDsl.g:217:2: ( ( rule__Primary__Alternatives ) )
            {
            // InternalExpressionsDsl.g:217:2: ( ( rule__Primary__Alternatives ) )
            // InternalExpressionsDsl.g:218:3: ( rule__Primary__Alternatives )
            {
             before(grammarAccess.getPrimaryAccess().getAlternatives()); 
            // InternalExpressionsDsl.g:219:3: ( rule__Primary__Alternatives )
            // InternalExpressionsDsl.g:219:4: rule__Primary__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleVariableRef"
    // InternalExpressionsDsl.g:228:1: entryRuleVariableRef : ruleVariableRef EOF ;
    public final void entryRuleVariableRef() throws RecognitionException {
        try {
            // InternalExpressionsDsl.g:229:1: ( ruleVariableRef EOF )
            // InternalExpressionsDsl.g:230:1: ruleVariableRef EOF
            {
             before(grammarAccess.getVariableRefRule()); 
            pushFollow(FOLLOW_1);
            ruleVariableRef();

            state._fsp--;

             after(grammarAccess.getVariableRefRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariableRef"


    // $ANTLR start "ruleVariableRef"
    // InternalExpressionsDsl.g:237:1: ruleVariableRef : ( ( rule__VariableRef__VariableAssignment ) ) ;
    public final void ruleVariableRef() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:241:2: ( ( ( rule__VariableRef__VariableAssignment ) ) )
            // InternalExpressionsDsl.g:242:2: ( ( rule__VariableRef__VariableAssignment ) )
            {
            // InternalExpressionsDsl.g:242:2: ( ( rule__VariableRef__VariableAssignment ) )
            // InternalExpressionsDsl.g:243:3: ( rule__VariableRef__VariableAssignment )
            {
             before(grammarAccess.getVariableRefAccess().getVariableAssignment()); 
            // InternalExpressionsDsl.g:244:3: ( rule__VariableRef__VariableAssignment )
            // InternalExpressionsDsl.g:244:4: rule__VariableRef__VariableAssignment
            {
            pushFollow(FOLLOW_2);
            rule__VariableRef__VariableAssignment();

            state._fsp--;


            }

             after(grammarAccess.getVariableRefAccess().getVariableAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariableRef"


    // $ANTLR start "entryRuleLiteral"
    // InternalExpressionsDsl.g:253:1: entryRuleLiteral : ruleLiteral EOF ;
    public final void entryRuleLiteral() throws RecognitionException {
        try {
            // InternalExpressionsDsl.g:254:1: ( ruleLiteral EOF )
            // InternalExpressionsDsl.g:255:1: ruleLiteral EOF
            {
             before(grammarAccess.getLiteralRule()); 
            pushFollow(FOLLOW_1);
            ruleLiteral();

            state._fsp--;

             after(grammarAccess.getLiteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLiteral"


    // $ANTLR start "ruleLiteral"
    // InternalExpressionsDsl.g:262:1: ruleLiteral : ( ( rule__Literal__ValueAssignment ) ) ;
    public final void ruleLiteral() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:266:2: ( ( ( rule__Literal__ValueAssignment ) ) )
            // InternalExpressionsDsl.g:267:2: ( ( rule__Literal__ValueAssignment ) )
            {
            // InternalExpressionsDsl.g:267:2: ( ( rule__Literal__ValueAssignment ) )
            // InternalExpressionsDsl.g:268:3: ( rule__Literal__ValueAssignment )
            {
             before(grammarAccess.getLiteralAccess().getValueAssignment()); 
            // InternalExpressionsDsl.g:269:3: ( rule__Literal__ValueAssignment )
            // InternalExpressionsDsl.g:269:4: rule__Literal__ValueAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Literal__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getLiteralAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLiteral"


    // $ANTLR start "rule__Statement__Alternatives"
    // InternalExpressionsDsl.g:277:1: rule__Statement__Alternatives : ( ( ruleAssignment ) | ( ruleAddition ) );
    public final void rule__Statement__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:281:1: ( ( ruleAssignment ) | ( ruleAddition ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==RULE_ID) ) {
                int LA1_1 = input.LA(2);

                if ( (LA1_1==EOF||(LA1_1>=RULE_ID && LA1_1<=RULE_INT)||(LA1_1>=11 && LA1_1<=14)||LA1_1==17) ) {
                    alt1=2;
                }
                else if ( (LA1_1==16) ) {
                    alt1=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA1_0==RULE_INT||LA1_0==17) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalExpressionsDsl.g:282:2: ( ruleAssignment )
                    {
                    // InternalExpressionsDsl.g:282:2: ( ruleAssignment )
                    // InternalExpressionsDsl.g:283:3: ruleAssignment
                    {
                     before(grammarAccess.getStatementAccess().getAssignmentParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleAssignment();

                    state._fsp--;

                     after(grammarAccess.getStatementAccess().getAssignmentParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalExpressionsDsl.g:288:2: ( ruleAddition )
                    {
                    // InternalExpressionsDsl.g:288:2: ( ruleAddition )
                    // InternalExpressionsDsl.g:289:3: ruleAddition
                    {
                     before(grammarAccess.getStatementAccess().getAdditionParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleAddition();

                    state._fsp--;

                     after(grammarAccess.getStatementAccess().getAdditionParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement__Alternatives"


    // $ANTLR start "rule__Addition__OpAlternatives_1_1_0"
    // InternalExpressionsDsl.g:298:1: rule__Addition__OpAlternatives_1_1_0 : ( ( '+' ) | ( '-' ) );
    public final void rule__Addition__OpAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:302:1: ( ( '+' ) | ( '-' ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                alt2=1;
            }
            else if ( (LA2_0==12) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalExpressionsDsl.g:303:2: ( '+' )
                    {
                    // InternalExpressionsDsl.g:303:2: ( '+' )
                    // InternalExpressionsDsl.g:304:3: '+'
                    {
                     before(grammarAccess.getAdditionAccess().getOpPlusSignKeyword_1_1_0_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getAdditionAccess().getOpPlusSignKeyword_1_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalExpressionsDsl.g:309:2: ( '-' )
                    {
                    // InternalExpressionsDsl.g:309:2: ( '-' )
                    // InternalExpressionsDsl.g:310:3: '-'
                    {
                     before(grammarAccess.getAdditionAccess().getOpHyphenMinusKeyword_1_1_0_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getAdditionAccess().getOpHyphenMinusKeyword_1_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__OpAlternatives_1_1_0"


    // $ANTLR start "rule__Multiplication__OpAlternatives_1_1_0"
    // InternalExpressionsDsl.g:319:1: rule__Multiplication__OpAlternatives_1_1_0 : ( ( '*' ) | ( '/' ) );
    public final void rule__Multiplication__OpAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:323:1: ( ( '*' ) | ( '/' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==13) ) {
                alt3=1;
            }
            else if ( (LA3_0==14) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalExpressionsDsl.g:324:2: ( '*' )
                    {
                    // InternalExpressionsDsl.g:324:2: ( '*' )
                    // InternalExpressionsDsl.g:325:3: '*'
                    {
                     before(grammarAccess.getMultiplicationAccess().getOpAsteriskKeyword_1_1_0_0()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getMultiplicationAccess().getOpAsteriskKeyword_1_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalExpressionsDsl.g:330:2: ( '/' )
                    {
                    // InternalExpressionsDsl.g:330:2: ( '/' )
                    // InternalExpressionsDsl.g:331:3: '/'
                    {
                     before(grammarAccess.getMultiplicationAccess().getOpSolidusKeyword_1_1_0_1()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getMultiplicationAccess().getOpSolidusKeyword_1_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Multiplication__OpAlternatives_1_1_0"


    // $ANTLR start "rule__Primary__Alternatives"
    // InternalExpressionsDsl.g:340:1: rule__Primary__Alternatives : ( ( ruleVariableRef ) | ( ruleLiteral ) | ( ( rule__Primary__Group_2__0 ) ) );
    public final void rule__Primary__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:344:1: ( ( ruleVariableRef ) | ( ruleLiteral ) | ( ( rule__Primary__Group_2__0 ) ) )
            int alt4=3;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt4=1;
                }
                break;
            case RULE_INT:
                {
                alt4=2;
                }
                break;
            case 17:
                {
                alt4=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalExpressionsDsl.g:345:2: ( ruleVariableRef )
                    {
                    // InternalExpressionsDsl.g:345:2: ( ruleVariableRef )
                    // InternalExpressionsDsl.g:346:3: ruleVariableRef
                    {
                     before(grammarAccess.getPrimaryAccess().getVariableRefParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleVariableRef();

                    state._fsp--;

                     after(grammarAccess.getPrimaryAccess().getVariableRefParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalExpressionsDsl.g:351:2: ( ruleLiteral )
                    {
                    // InternalExpressionsDsl.g:351:2: ( ruleLiteral )
                    // InternalExpressionsDsl.g:352:3: ruleLiteral
                    {
                     before(grammarAccess.getPrimaryAccess().getLiteralParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleLiteral();

                    state._fsp--;

                     after(grammarAccess.getPrimaryAccess().getLiteralParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalExpressionsDsl.g:357:2: ( ( rule__Primary__Group_2__0 ) )
                    {
                    // InternalExpressionsDsl.g:357:2: ( ( rule__Primary__Group_2__0 ) )
                    // InternalExpressionsDsl.g:358:3: ( rule__Primary__Group_2__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_2()); 
                    // InternalExpressionsDsl.g:359:3: ( rule__Primary__Group_2__0 )
                    // InternalExpressionsDsl.g:359:4: rule__Primary__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Alternatives"


    // $ANTLR start "rule__Model__Group__0"
    // InternalExpressionsDsl.g:367:1: rule__Model__Group__0 : rule__Model__Group__0__Impl rule__Model__Group__1 ;
    public final void rule__Model__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:371:1: ( rule__Model__Group__0__Impl rule__Model__Group__1 )
            // InternalExpressionsDsl.g:372:2: rule__Model__Group__0__Impl rule__Model__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Model__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0"


    // $ANTLR start "rule__Model__Group__0__Impl"
    // InternalExpressionsDsl.g:379:1: rule__Model__Group__0__Impl : ( ( rule__Model__VariablesAssignment_0 )* ) ;
    public final void rule__Model__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:383:1: ( ( ( rule__Model__VariablesAssignment_0 )* ) )
            // InternalExpressionsDsl.g:384:1: ( ( rule__Model__VariablesAssignment_0 )* )
            {
            // InternalExpressionsDsl.g:384:1: ( ( rule__Model__VariablesAssignment_0 )* )
            // InternalExpressionsDsl.g:385:2: ( rule__Model__VariablesAssignment_0 )*
            {
             before(grammarAccess.getModelAccess().getVariablesAssignment_0()); 
            // InternalExpressionsDsl.g:386:2: ( rule__Model__VariablesAssignment_0 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==15) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalExpressionsDsl.g:386:3: rule__Model__VariablesAssignment_0
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__Model__VariablesAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getVariablesAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0__Impl"


    // $ANTLR start "rule__Model__Group__1"
    // InternalExpressionsDsl.g:394:1: rule__Model__Group__1 : rule__Model__Group__1__Impl ;
    public final void rule__Model__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:398:1: ( rule__Model__Group__1__Impl )
            // InternalExpressionsDsl.g:399:2: rule__Model__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Model__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1"


    // $ANTLR start "rule__Model__Group__1__Impl"
    // InternalExpressionsDsl.g:405:1: rule__Model__Group__1__Impl : ( ( rule__Model__StatementsAssignment_1 )* ) ;
    public final void rule__Model__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:409:1: ( ( ( rule__Model__StatementsAssignment_1 )* ) )
            // InternalExpressionsDsl.g:410:1: ( ( rule__Model__StatementsAssignment_1 )* )
            {
            // InternalExpressionsDsl.g:410:1: ( ( rule__Model__StatementsAssignment_1 )* )
            // InternalExpressionsDsl.g:411:2: ( rule__Model__StatementsAssignment_1 )*
            {
             before(grammarAccess.getModelAccess().getStatementsAssignment_1()); 
            // InternalExpressionsDsl.g:412:2: ( rule__Model__StatementsAssignment_1 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>=RULE_ID && LA6_0<=RULE_INT)||LA6_0==17) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalExpressionsDsl.g:412:3: rule__Model__StatementsAssignment_1
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Model__StatementsAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getStatementsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1__Impl"


    // $ANTLR start "rule__Variable__Group__0"
    // InternalExpressionsDsl.g:421:1: rule__Variable__Group__0 : rule__Variable__Group__0__Impl rule__Variable__Group__1 ;
    public final void rule__Variable__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:425:1: ( rule__Variable__Group__0__Impl rule__Variable__Group__1 )
            // InternalExpressionsDsl.g:426:2: rule__Variable__Group__0__Impl rule__Variable__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Variable__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variable__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group__0"


    // $ANTLR start "rule__Variable__Group__0__Impl"
    // InternalExpressionsDsl.g:433:1: rule__Variable__Group__0__Impl : ( 'var' ) ;
    public final void rule__Variable__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:437:1: ( ( 'var' ) )
            // InternalExpressionsDsl.g:438:1: ( 'var' )
            {
            // InternalExpressionsDsl.g:438:1: ( 'var' )
            // InternalExpressionsDsl.g:439:2: 'var'
            {
             before(grammarAccess.getVariableAccess().getVarKeyword_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getVariableAccess().getVarKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group__0__Impl"


    // $ANTLR start "rule__Variable__Group__1"
    // InternalExpressionsDsl.g:448:1: rule__Variable__Group__1 : rule__Variable__Group__1__Impl ;
    public final void rule__Variable__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:452:1: ( rule__Variable__Group__1__Impl )
            // InternalExpressionsDsl.g:453:2: rule__Variable__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Variable__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group__1"


    // $ANTLR start "rule__Variable__Group__1__Impl"
    // InternalExpressionsDsl.g:459:1: rule__Variable__Group__1__Impl : ( ( rule__Variable__NameAssignment_1 ) ) ;
    public final void rule__Variable__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:463:1: ( ( ( rule__Variable__NameAssignment_1 ) ) )
            // InternalExpressionsDsl.g:464:1: ( ( rule__Variable__NameAssignment_1 ) )
            {
            // InternalExpressionsDsl.g:464:1: ( ( rule__Variable__NameAssignment_1 ) )
            // InternalExpressionsDsl.g:465:2: ( rule__Variable__NameAssignment_1 )
            {
             before(grammarAccess.getVariableAccess().getNameAssignment_1()); 
            // InternalExpressionsDsl.g:466:2: ( rule__Variable__NameAssignment_1 )
            // InternalExpressionsDsl.g:466:3: rule__Variable__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Variable__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getVariableAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group__1__Impl"


    // $ANTLR start "rule__Assignment__Group__0"
    // InternalExpressionsDsl.g:475:1: rule__Assignment__Group__0 : rule__Assignment__Group__0__Impl rule__Assignment__Group__1 ;
    public final void rule__Assignment__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:479:1: ( rule__Assignment__Group__0__Impl rule__Assignment__Group__1 )
            // InternalExpressionsDsl.g:480:2: rule__Assignment__Group__0__Impl rule__Assignment__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Assignment__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Assignment__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__0"


    // $ANTLR start "rule__Assignment__Group__0__Impl"
    // InternalExpressionsDsl.g:487:1: rule__Assignment__Group__0__Impl : ( ( rule__Assignment__VariableAssignment_0 ) ) ;
    public final void rule__Assignment__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:491:1: ( ( ( rule__Assignment__VariableAssignment_0 ) ) )
            // InternalExpressionsDsl.g:492:1: ( ( rule__Assignment__VariableAssignment_0 ) )
            {
            // InternalExpressionsDsl.g:492:1: ( ( rule__Assignment__VariableAssignment_0 ) )
            // InternalExpressionsDsl.g:493:2: ( rule__Assignment__VariableAssignment_0 )
            {
             before(grammarAccess.getAssignmentAccess().getVariableAssignment_0()); 
            // InternalExpressionsDsl.g:494:2: ( rule__Assignment__VariableAssignment_0 )
            // InternalExpressionsDsl.g:494:3: rule__Assignment__VariableAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Assignment__VariableAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getAssignmentAccess().getVariableAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__0__Impl"


    // $ANTLR start "rule__Assignment__Group__1"
    // InternalExpressionsDsl.g:502:1: rule__Assignment__Group__1 : rule__Assignment__Group__1__Impl rule__Assignment__Group__2 ;
    public final void rule__Assignment__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:506:1: ( rule__Assignment__Group__1__Impl rule__Assignment__Group__2 )
            // InternalExpressionsDsl.g:507:2: rule__Assignment__Group__1__Impl rule__Assignment__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__Assignment__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Assignment__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__1"


    // $ANTLR start "rule__Assignment__Group__1__Impl"
    // InternalExpressionsDsl.g:514:1: rule__Assignment__Group__1__Impl : ( '=' ) ;
    public final void rule__Assignment__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:518:1: ( ( '=' ) )
            // InternalExpressionsDsl.g:519:1: ( '=' )
            {
            // InternalExpressionsDsl.g:519:1: ( '=' )
            // InternalExpressionsDsl.g:520:2: '='
            {
             before(grammarAccess.getAssignmentAccess().getEqualsSignKeyword_1()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getAssignmentAccess().getEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__1__Impl"


    // $ANTLR start "rule__Assignment__Group__2"
    // InternalExpressionsDsl.g:529:1: rule__Assignment__Group__2 : rule__Assignment__Group__2__Impl ;
    public final void rule__Assignment__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:533:1: ( rule__Assignment__Group__2__Impl )
            // InternalExpressionsDsl.g:534:2: rule__Assignment__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Assignment__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__2"


    // $ANTLR start "rule__Assignment__Group__2__Impl"
    // InternalExpressionsDsl.g:540:1: rule__Assignment__Group__2__Impl : ( ( rule__Assignment__ValueAssignment_2 ) ) ;
    public final void rule__Assignment__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:544:1: ( ( ( rule__Assignment__ValueAssignment_2 ) ) )
            // InternalExpressionsDsl.g:545:1: ( ( rule__Assignment__ValueAssignment_2 ) )
            {
            // InternalExpressionsDsl.g:545:1: ( ( rule__Assignment__ValueAssignment_2 ) )
            // InternalExpressionsDsl.g:546:2: ( rule__Assignment__ValueAssignment_2 )
            {
             before(grammarAccess.getAssignmentAccess().getValueAssignment_2()); 
            // InternalExpressionsDsl.g:547:2: ( rule__Assignment__ValueAssignment_2 )
            // InternalExpressionsDsl.g:547:3: rule__Assignment__ValueAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Assignment__ValueAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getAssignmentAccess().getValueAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__2__Impl"


    // $ANTLR start "rule__Addition__Group__0"
    // InternalExpressionsDsl.g:556:1: rule__Addition__Group__0 : rule__Addition__Group__0__Impl rule__Addition__Group__1 ;
    public final void rule__Addition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:560:1: ( rule__Addition__Group__0__Impl rule__Addition__Group__1 )
            // InternalExpressionsDsl.g:561:2: rule__Addition__Group__0__Impl rule__Addition__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Addition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Addition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group__0"


    // $ANTLR start "rule__Addition__Group__0__Impl"
    // InternalExpressionsDsl.g:568:1: rule__Addition__Group__0__Impl : ( ruleMultiplication ) ;
    public final void rule__Addition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:572:1: ( ( ruleMultiplication ) )
            // InternalExpressionsDsl.g:573:1: ( ruleMultiplication )
            {
            // InternalExpressionsDsl.g:573:1: ( ruleMultiplication )
            // InternalExpressionsDsl.g:574:2: ruleMultiplication
            {
             before(grammarAccess.getAdditionAccess().getMultiplicationParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleMultiplication();

            state._fsp--;

             after(grammarAccess.getAdditionAccess().getMultiplicationParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group__0__Impl"


    // $ANTLR start "rule__Addition__Group__1"
    // InternalExpressionsDsl.g:583:1: rule__Addition__Group__1 : rule__Addition__Group__1__Impl ;
    public final void rule__Addition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:587:1: ( rule__Addition__Group__1__Impl )
            // InternalExpressionsDsl.g:588:2: rule__Addition__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Addition__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group__1"


    // $ANTLR start "rule__Addition__Group__1__Impl"
    // InternalExpressionsDsl.g:594:1: rule__Addition__Group__1__Impl : ( ( rule__Addition__Group_1__0 )* ) ;
    public final void rule__Addition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:598:1: ( ( ( rule__Addition__Group_1__0 )* ) )
            // InternalExpressionsDsl.g:599:1: ( ( rule__Addition__Group_1__0 )* )
            {
            // InternalExpressionsDsl.g:599:1: ( ( rule__Addition__Group_1__0 )* )
            // InternalExpressionsDsl.g:600:2: ( rule__Addition__Group_1__0 )*
            {
             before(grammarAccess.getAdditionAccess().getGroup_1()); 
            // InternalExpressionsDsl.g:601:2: ( rule__Addition__Group_1__0 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>=11 && LA7_0<=12)) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalExpressionsDsl.g:601:3: rule__Addition__Group_1__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Addition__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getAdditionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group__1__Impl"


    // $ANTLR start "rule__Addition__Group_1__0"
    // InternalExpressionsDsl.g:610:1: rule__Addition__Group_1__0 : rule__Addition__Group_1__0__Impl rule__Addition__Group_1__1 ;
    public final void rule__Addition__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:614:1: ( rule__Addition__Group_1__0__Impl rule__Addition__Group_1__1 )
            // InternalExpressionsDsl.g:615:2: rule__Addition__Group_1__0__Impl rule__Addition__Group_1__1
            {
            pushFollow(FOLLOW_8);
            rule__Addition__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Addition__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group_1__0"


    // $ANTLR start "rule__Addition__Group_1__0__Impl"
    // InternalExpressionsDsl.g:622:1: rule__Addition__Group_1__0__Impl : ( () ) ;
    public final void rule__Addition__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:626:1: ( ( () ) )
            // InternalExpressionsDsl.g:627:1: ( () )
            {
            // InternalExpressionsDsl.g:627:1: ( () )
            // InternalExpressionsDsl.g:628:2: ()
            {
             before(grammarAccess.getAdditionAccess().getAdditionOp1Action_1_0()); 
            // InternalExpressionsDsl.g:629:2: ()
            // InternalExpressionsDsl.g:629:3: 
            {
            }

             after(grammarAccess.getAdditionAccess().getAdditionOp1Action_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group_1__0__Impl"


    // $ANTLR start "rule__Addition__Group_1__1"
    // InternalExpressionsDsl.g:637:1: rule__Addition__Group_1__1 : rule__Addition__Group_1__1__Impl rule__Addition__Group_1__2 ;
    public final void rule__Addition__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:641:1: ( rule__Addition__Group_1__1__Impl rule__Addition__Group_1__2 )
            // InternalExpressionsDsl.g:642:2: rule__Addition__Group_1__1__Impl rule__Addition__Group_1__2
            {
            pushFollow(FOLLOW_3);
            rule__Addition__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Addition__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group_1__1"


    // $ANTLR start "rule__Addition__Group_1__1__Impl"
    // InternalExpressionsDsl.g:649:1: rule__Addition__Group_1__1__Impl : ( ( rule__Addition__OpAssignment_1_1 ) ) ;
    public final void rule__Addition__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:653:1: ( ( ( rule__Addition__OpAssignment_1_1 ) ) )
            // InternalExpressionsDsl.g:654:1: ( ( rule__Addition__OpAssignment_1_1 ) )
            {
            // InternalExpressionsDsl.g:654:1: ( ( rule__Addition__OpAssignment_1_1 ) )
            // InternalExpressionsDsl.g:655:2: ( rule__Addition__OpAssignment_1_1 )
            {
             before(grammarAccess.getAdditionAccess().getOpAssignment_1_1()); 
            // InternalExpressionsDsl.g:656:2: ( rule__Addition__OpAssignment_1_1 )
            // InternalExpressionsDsl.g:656:3: rule__Addition__OpAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Addition__OpAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getAdditionAccess().getOpAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group_1__1__Impl"


    // $ANTLR start "rule__Addition__Group_1__2"
    // InternalExpressionsDsl.g:664:1: rule__Addition__Group_1__2 : rule__Addition__Group_1__2__Impl ;
    public final void rule__Addition__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:668:1: ( rule__Addition__Group_1__2__Impl )
            // InternalExpressionsDsl.g:669:2: rule__Addition__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Addition__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group_1__2"


    // $ANTLR start "rule__Addition__Group_1__2__Impl"
    // InternalExpressionsDsl.g:675:1: rule__Addition__Group_1__2__Impl : ( ( rule__Addition__Op2Assignment_1_2 ) ) ;
    public final void rule__Addition__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:679:1: ( ( ( rule__Addition__Op2Assignment_1_2 ) ) )
            // InternalExpressionsDsl.g:680:1: ( ( rule__Addition__Op2Assignment_1_2 ) )
            {
            // InternalExpressionsDsl.g:680:1: ( ( rule__Addition__Op2Assignment_1_2 ) )
            // InternalExpressionsDsl.g:681:2: ( rule__Addition__Op2Assignment_1_2 )
            {
             before(grammarAccess.getAdditionAccess().getOp2Assignment_1_2()); 
            // InternalExpressionsDsl.g:682:2: ( rule__Addition__Op2Assignment_1_2 )
            // InternalExpressionsDsl.g:682:3: rule__Addition__Op2Assignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Addition__Op2Assignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAdditionAccess().getOp2Assignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group_1__2__Impl"


    // $ANTLR start "rule__Multiplication__Group__0"
    // InternalExpressionsDsl.g:691:1: rule__Multiplication__Group__0 : rule__Multiplication__Group__0__Impl rule__Multiplication__Group__1 ;
    public final void rule__Multiplication__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:695:1: ( rule__Multiplication__Group__0__Impl rule__Multiplication__Group__1 )
            // InternalExpressionsDsl.g:696:2: rule__Multiplication__Group__0__Impl rule__Multiplication__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__Multiplication__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Multiplication__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Multiplication__Group__0"


    // $ANTLR start "rule__Multiplication__Group__0__Impl"
    // InternalExpressionsDsl.g:703:1: rule__Multiplication__Group__0__Impl : ( rulePrimary ) ;
    public final void rule__Multiplication__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:707:1: ( ( rulePrimary ) )
            // InternalExpressionsDsl.g:708:1: ( rulePrimary )
            {
            // InternalExpressionsDsl.g:708:1: ( rulePrimary )
            // InternalExpressionsDsl.g:709:2: rulePrimary
            {
             before(grammarAccess.getMultiplicationAccess().getPrimaryParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getMultiplicationAccess().getPrimaryParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Multiplication__Group__0__Impl"


    // $ANTLR start "rule__Multiplication__Group__1"
    // InternalExpressionsDsl.g:718:1: rule__Multiplication__Group__1 : rule__Multiplication__Group__1__Impl ;
    public final void rule__Multiplication__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:722:1: ( rule__Multiplication__Group__1__Impl )
            // InternalExpressionsDsl.g:723:2: rule__Multiplication__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Multiplication__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Multiplication__Group__1"


    // $ANTLR start "rule__Multiplication__Group__1__Impl"
    // InternalExpressionsDsl.g:729:1: rule__Multiplication__Group__1__Impl : ( ( rule__Multiplication__Group_1__0 )* ) ;
    public final void rule__Multiplication__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:733:1: ( ( ( rule__Multiplication__Group_1__0 )* ) )
            // InternalExpressionsDsl.g:734:1: ( ( rule__Multiplication__Group_1__0 )* )
            {
            // InternalExpressionsDsl.g:734:1: ( ( rule__Multiplication__Group_1__0 )* )
            // InternalExpressionsDsl.g:735:2: ( rule__Multiplication__Group_1__0 )*
            {
             before(grammarAccess.getMultiplicationAccess().getGroup_1()); 
            // InternalExpressionsDsl.g:736:2: ( rule__Multiplication__Group_1__0 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>=13 && LA8_0<=14)) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalExpressionsDsl.g:736:3: rule__Multiplication__Group_1__0
            	    {
            	    pushFollow(FOLLOW_11);
            	    rule__Multiplication__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getMultiplicationAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Multiplication__Group__1__Impl"


    // $ANTLR start "rule__Multiplication__Group_1__0"
    // InternalExpressionsDsl.g:745:1: rule__Multiplication__Group_1__0 : rule__Multiplication__Group_1__0__Impl rule__Multiplication__Group_1__1 ;
    public final void rule__Multiplication__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:749:1: ( rule__Multiplication__Group_1__0__Impl rule__Multiplication__Group_1__1 )
            // InternalExpressionsDsl.g:750:2: rule__Multiplication__Group_1__0__Impl rule__Multiplication__Group_1__1
            {
            pushFollow(FOLLOW_10);
            rule__Multiplication__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Multiplication__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Multiplication__Group_1__0"


    // $ANTLR start "rule__Multiplication__Group_1__0__Impl"
    // InternalExpressionsDsl.g:757:1: rule__Multiplication__Group_1__0__Impl : ( () ) ;
    public final void rule__Multiplication__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:761:1: ( ( () ) )
            // InternalExpressionsDsl.g:762:1: ( () )
            {
            // InternalExpressionsDsl.g:762:1: ( () )
            // InternalExpressionsDsl.g:763:2: ()
            {
             before(grammarAccess.getMultiplicationAccess().getMultiplicationOp1Action_1_0()); 
            // InternalExpressionsDsl.g:764:2: ()
            // InternalExpressionsDsl.g:764:3: 
            {
            }

             after(grammarAccess.getMultiplicationAccess().getMultiplicationOp1Action_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Multiplication__Group_1__0__Impl"


    // $ANTLR start "rule__Multiplication__Group_1__1"
    // InternalExpressionsDsl.g:772:1: rule__Multiplication__Group_1__1 : rule__Multiplication__Group_1__1__Impl rule__Multiplication__Group_1__2 ;
    public final void rule__Multiplication__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:776:1: ( rule__Multiplication__Group_1__1__Impl rule__Multiplication__Group_1__2 )
            // InternalExpressionsDsl.g:777:2: rule__Multiplication__Group_1__1__Impl rule__Multiplication__Group_1__2
            {
            pushFollow(FOLLOW_3);
            rule__Multiplication__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Multiplication__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Multiplication__Group_1__1"


    // $ANTLR start "rule__Multiplication__Group_1__1__Impl"
    // InternalExpressionsDsl.g:784:1: rule__Multiplication__Group_1__1__Impl : ( ( rule__Multiplication__OpAssignment_1_1 ) ) ;
    public final void rule__Multiplication__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:788:1: ( ( ( rule__Multiplication__OpAssignment_1_1 ) ) )
            // InternalExpressionsDsl.g:789:1: ( ( rule__Multiplication__OpAssignment_1_1 ) )
            {
            // InternalExpressionsDsl.g:789:1: ( ( rule__Multiplication__OpAssignment_1_1 ) )
            // InternalExpressionsDsl.g:790:2: ( rule__Multiplication__OpAssignment_1_1 )
            {
             before(grammarAccess.getMultiplicationAccess().getOpAssignment_1_1()); 
            // InternalExpressionsDsl.g:791:2: ( rule__Multiplication__OpAssignment_1_1 )
            // InternalExpressionsDsl.g:791:3: rule__Multiplication__OpAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Multiplication__OpAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getMultiplicationAccess().getOpAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Multiplication__Group_1__1__Impl"


    // $ANTLR start "rule__Multiplication__Group_1__2"
    // InternalExpressionsDsl.g:799:1: rule__Multiplication__Group_1__2 : rule__Multiplication__Group_1__2__Impl ;
    public final void rule__Multiplication__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:803:1: ( rule__Multiplication__Group_1__2__Impl )
            // InternalExpressionsDsl.g:804:2: rule__Multiplication__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Multiplication__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Multiplication__Group_1__2"


    // $ANTLR start "rule__Multiplication__Group_1__2__Impl"
    // InternalExpressionsDsl.g:810:1: rule__Multiplication__Group_1__2__Impl : ( ( rule__Multiplication__Op2Assignment_1_2 ) ) ;
    public final void rule__Multiplication__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:814:1: ( ( ( rule__Multiplication__Op2Assignment_1_2 ) ) )
            // InternalExpressionsDsl.g:815:1: ( ( rule__Multiplication__Op2Assignment_1_2 ) )
            {
            // InternalExpressionsDsl.g:815:1: ( ( rule__Multiplication__Op2Assignment_1_2 ) )
            // InternalExpressionsDsl.g:816:2: ( rule__Multiplication__Op2Assignment_1_2 )
            {
             before(grammarAccess.getMultiplicationAccess().getOp2Assignment_1_2()); 
            // InternalExpressionsDsl.g:817:2: ( rule__Multiplication__Op2Assignment_1_2 )
            // InternalExpressionsDsl.g:817:3: rule__Multiplication__Op2Assignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Multiplication__Op2Assignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getMultiplicationAccess().getOp2Assignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Multiplication__Group_1__2__Impl"


    // $ANTLR start "rule__Primary__Group_2__0"
    // InternalExpressionsDsl.g:826:1: rule__Primary__Group_2__0 : rule__Primary__Group_2__0__Impl rule__Primary__Group_2__1 ;
    public final void rule__Primary__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:830:1: ( rule__Primary__Group_2__0__Impl rule__Primary__Group_2__1 )
            // InternalExpressionsDsl.g:831:2: rule__Primary__Group_2__0__Impl rule__Primary__Group_2__1
            {
            pushFollow(FOLLOW_3);
            rule__Primary__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__0"


    // $ANTLR start "rule__Primary__Group_2__0__Impl"
    // InternalExpressionsDsl.g:838:1: rule__Primary__Group_2__0__Impl : ( '(' ) ;
    public final void rule__Primary__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:842:1: ( ( '(' ) )
            // InternalExpressionsDsl.g:843:1: ( '(' )
            {
            // InternalExpressionsDsl.g:843:1: ( '(' )
            // InternalExpressionsDsl.g:844:2: '('
            {
             before(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_2_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__0__Impl"


    // $ANTLR start "rule__Primary__Group_2__1"
    // InternalExpressionsDsl.g:853:1: rule__Primary__Group_2__1 : rule__Primary__Group_2__1__Impl rule__Primary__Group_2__2 ;
    public final void rule__Primary__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:857:1: ( rule__Primary__Group_2__1__Impl rule__Primary__Group_2__2 )
            // InternalExpressionsDsl.g:858:2: rule__Primary__Group_2__1__Impl rule__Primary__Group_2__2
            {
            pushFollow(FOLLOW_12);
            rule__Primary__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__1"


    // $ANTLR start "rule__Primary__Group_2__1__Impl"
    // InternalExpressionsDsl.g:865:1: rule__Primary__Group_2__1__Impl : ( ruleAddition ) ;
    public final void rule__Primary__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:869:1: ( ( ruleAddition ) )
            // InternalExpressionsDsl.g:870:1: ( ruleAddition )
            {
            // InternalExpressionsDsl.g:870:1: ( ruleAddition )
            // InternalExpressionsDsl.g:871:2: ruleAddition
            {
             before(grammarAccess.getPrimaryAccess().getAdditionParserRuleCall_2_1()); 
            pushFollow(FOLLOW_2);
            ruleAddition();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getAdditionParserRuleCall_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__1__Impl"


    // $ANTLR start "rule__Primary__Group_2__2"
    // InternalExpressionsDsl.g:880:1: rule__Primary__Group_2__2 : rule__Primary__Group_2__2__Impl ;
    public final void rule__Primary__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:884:1: ( rule__Primary__Group_2__2__Impl )
            // InternalExpressionsDsl.g:885:2: rule__Primary__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__2"


    // $ANTLR start "rule__Primary__Group_2__2__Impl"
    // InternalExpressionsDsl.g:891:1: rule__Primary__Group_2__2__Impl : ( ')' ) ;
    public final void rule__Primary__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:895:1: ( ( ')' ) )
            // InternalExpressionsDsl.g:896:1: ( ')' )
            {
            // InternalExpressionsDsl.g:896:1: ( ')' )
            // InternalExpressionsDsl.g:897:2: ')'
            {
             before(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_2_2()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_2__2__Impl"


    // $ANTLR start "rule__Model__VariablesAssignment_0"
    // InternalExpressionsDsl.g:907:1: rule__Model__VariablesAssignment_0 : ( ruleVariable ) ;
    public final void rule__Model__VariablesAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:911:1: ( ( ruleVariable ) )
            // InternalExpressionsDsl.g:912:2: ( ruleVariable )
            {
            // InternalExpressionsDsl.g:912:2: ( ruleVariable )
            // InternalExpressionsDsl.g:913:3: ruleVariable
            {
             before(grammarAccess.getModelAccess().getVariablesVariableParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleVariable();

            state._fsp--;

             after(grammarAccess.getModelAccess().getVariablesVariableParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__VariablesAssignment_0"


    // $ANTLR start "rule__Model__StatementsAssignment_1"
    // InternalExpressionsDsl.g:922:1: rule__Model__StatementsAssignment_1 : ( ruleStatement ) ;
    public final void rule__Model__StatementsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:926:1: ( ( ruleStatement ) )
            // InternalExpressionsDsl.g:927:2: ( ruleStatement )
            {
            // InternalExpressionsDsl.g:927:2: ( ruleStatement )
            // InternalExpressionsDsl.g:928:3: ruleStatement
            {
             before(grammarAccess.getModelAccess().getStatementsStatementParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleStatement();

            state._fsp--;

             after(grammarAccess.getModelAccess().getStatementsStatementParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__StatementsAssignment_1"


    // $ANTLR start "rule__Variable__NameAssignment_1"
    // InternalExpressionsDsl.g:937:1: rule__Variable__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Variable__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:941:1: ( ( RULE_ID ) )
            // InternalExpressionsDsl.g:942:2: ( RULE_ID )
            {
            // InternalExpressionsDsl.g:942:2: ( RULE_ID )
            // InternalExpressionsDsl.g:943:3: RULE_ID
            {
             before(grammarAccess.getVariableAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getVariableAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__NameAssignment_1"


    // $ANTLR start "rule__Assignment__VariableAssignment_0"
    // InternalExpressionsDsl.g:952:1: rule__Assignment__VariableAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__Assignment__VariableAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:956:1: ( ( ( RULE_ID ) ) )
            // InternalExpressionsDsl.g:957:2: ( ( RULE_ID ) )
            {
            // InternalExpressionsDsl.g:957:2: ( ( RULE_ID ) )
            // InternalExpressionsDsl.g:958:3: ( RULE_ID )
            {
             before(grammarAccess.getAssignmentAccess().getVariableVariableCrossReference_0_0()); 
            // InternalExpressionsDsl.g:959:3: ( RULE_ID )
            // InternalExpressionsDsl.g:960:4: RULE_ID
            {
             before(grammarAccess.getAssignmentAccess().getVariableVariableIDTerminalRuleCall_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAssignmentAccess().getVariableVariableIDTerminalRuleCall_0_0_1()); 

            }

             after(grammarAccess.getAssignmentAccess().getVariableVariableCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__VariableAssignment_0"


    // $ANTLR start "rule__Assignment__ValueAssignment_2"
    // InternalExpressionsDsl.g:971:1: rule__Assignment__ValueAssignment_2 : ( ruleAddition ) ;
    public final void rule__Assignment__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:975:1: ( ( ruleAddition ) )
            // InternalExpressionsDsl.g:976:2: ( ruleAddition )
            {
            // InternalExpressionsDsl.g:976:2: ( ruleAddition )
            // InternalExpressionsDsl.g:977:3: ruleAddition
            {
             before(grammarAccess.getAssignmentAccess().getValueAdditionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAddition();

            state._fsp--;

             after(grammarAccess.getAssignmentAccess().getValueAdditionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__ValueAssignment_2"


    // $ANTLR start "rule__Addition__OpAssignment_1_1"
    // InternalExpressionsDsl.g:986:1: rule__Addition__OpAssignment_1_1 : ( ( rule__Addition__OpAlternatives_1_1_0 ) ) ;
    public final void rule__Addition__OpAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:990:1: ( ( ( rule__Addition__OpAlternatives_1_1_0 ) ) )
            // InternalExpressionsDsl.g:991:2: ( ( rule__Addition__OpAlternatives_1_1_0 ) )
            {
            // InternalExpressionsDsl.g:991:2: ( ( rule__Addition__OpAlternatives_1_1_0 ) )
            // InternalExpressionsDsl.g:992:3: ( rule__Addition__OpAlternatives_1_1_0 )
            {
             before(grammarAccess.getAdditionAccess().getOpAlternatives_1_1_0()); 
            // InternalExpressionsDsl.g:993:3: ( rule__Addition__OpAlternatives_1_1_0 )
            // InternalExpressionsDsl.g:993:4: rule__Addition__OpAlternatives_1_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Addition__OpAlternatives_1_1_0();

            state._fsp--;


            }

             after(grammarAccess.getAdditionAccess().getOpAlternatives_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__OpAssignment_1_1"


    // $ANTLR start "rule__Addition__Op2Assignment_1_2"
    // InternalExpressionsDsl.g:1001:1: rule__Addition__Op2Assignment_1_2 : ( ruleMultiplication ) ;
    public final void rule__Addition__Op2Assignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:1005:1: ( ( ruleMultiplication ) )
            // InternalExpressionsDsl.g:1006:2: ( ruleMultiplication )
            {
            // InternalExpressionsDsl.g:1006:2: ( ruleMultiplication )
            // InternalExpressionsDsl.g:1007:3: ruleMultiplication
            {
             before(grammarAccess.getAdditionAccess().getOp2MultiplicationParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleMultiplication();

            state._fsp--;

             after(grammarAccess.getAdditionAccess().getOp2MultiplicationParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Op2Assignment_1_2"


    // $ANTLR start "rule__Multiplication__OpAssignment_1_1"
    // InternalExpressionsDsl.g:1016:1: rule__Multiplication__OpAssignment_1_1 : ( ( rule__Multiplication__OpAlternatives_1_1_0 ) ) ;
    public final void rule__Multiplication__OpAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:1020:1: ( ( ( rule__Multiplication__OpAlternatives_1_1_0 ) ) )
            // InternalExpressionsDsl.g:1021:2: ( ( rule__Multiplication__OpAlternatives_1_1_0 ) )
            {
            // InternalExpressionsDsl.g:1021:2: ( ( rule__Multiplication__OpAlternatives_1_1_0 ) )
            // InternalExpressionsDsl.g:1022:3: ( rule__Multiplication__OpAlternatives_1_1_0 )
            {
             before(grammarAccess.getMultiplicationAccess().getOpAlternatives_1_1_0()); 
            // InternalExpressionsDsl.g:1023:3: ( rule__Multiplication__OpAlternatives_1_1_0 )
            // InternalExpressionsDsl.g:1023:4: rule__Multiplication__OpAlternatives_1_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Multiplication__OpAlternatives_1_1_0();

            state._fsp--;


            }

             after(grammarAccess.getMultiplicationAccess().getOpAlternatives_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Multiplication__OpAssignment_1_1"


    // $ANTLR start "rule__Multiplication__Op2Assignment_1_2"
    // InternalExpressionsDsl.g:1031:1: rule__Multiplication__Op2Assignment_1_2 : ( rulePrimary ) ;
    public final void rule__Multiplication__Op2Assignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:1035:1: ( ( rulePrimary ) )
            // InternalExpressionsDsl.g:1036:2: ( rulePrimary )
            {
            // InternalExpressionsDsl.g:1036:2: ( rulePrimary )
            // InternalExpressionsDsl.g:1037:3: rulePrimary
            {
             before(grammarAccess.getMultiplicationAccess().getOp2PrimaryParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getMultiplicationAccess().getOp2PrimaryParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Multiplication__Op2Assignment_1_2"


    // $ANTLR start "rule__VariableRef__VariableAssignment"
    // InternalExpressionsDsl.g:1046:1: rule__VariableRef__VariableAssignment : ( ( RULE_ID ) ) ;
    public final void rule__VariableRef__VariableAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:1050:1: ( ( ( RULE_ID ) ) )
            // InternalExpressionsDsl.g:1051:2: ( ( RULE_ID ) )
            {
            // InternalExpressionsDsl.g:1051:2: ( ( RULE_ID ) )
            // InternalExpressionsDsl.g:1052:3: ( RULE_ID )
            {
             before(grammarAccess.getVariableRefAccess().getVariableVariableCrossReference_0()); 
            // InternalExpressionsDsl.g:1053:3: ( RULE_ID )
            // InternalExpressionsDsl.g:1054:4: RULE_ID
            {
             before(grammarAccess.getVariableRefAccess().getVariableVariableIDTerminalRuleCall_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getVariableRefAccess().getVariableVariableIDTerminalRuleCall_0_1()); 

            }

             after(grammarAccess.getVariableRefAccess().getVariableVariableCrossReference_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableRef__VariableAssignment"


    // $ANTLR start "rule__Literal__ValueAssignment"
    // InternalExpressionsDsl.g:1065:1: rule__Literal__ValueAssignment : ( RULE_INT ) ;
    public final void rule__Literal__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalExpressionsDsl.g:1069:1: ( ( RULE_INT ) )
            // InternalExpressionsDsl.g:1070:2: ( RULE_INT )
            {
            // InternalExpressionsDsl.g:1070:2: ( RULE_INT )
            // InternalExpressionsDsl.g:1071:3: RULE_INT
            {
             before(grammarAccess.getLiteralAccess().getValueINTTerminalRuleCall_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getLiteralAccess().getValueINTTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__ValueAssignment"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000020030L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000020032L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000001802L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000006002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000040000L});

}