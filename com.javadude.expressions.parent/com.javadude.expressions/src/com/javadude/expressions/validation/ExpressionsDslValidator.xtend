/*
 * generated by Xtext 2.14.0
 */
package com.javadude.expressions.validation

import org.eclipse.xtext.validation.Check
import com.javadude.expressions.expressionsDsl.Variable
import com.javadude.expressions.expressionsDsl.ExpressionsDslPackage

/**
 * This class contains custom validation rules. 
 *
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
class ExpressionsDslValidator extends AbstractExpressionsDslValidator {
	
	public static val INVALID_NAME = 'invalidName'

	@Check
	def variablesStartWithLowercase(Variable variable) {
		if (Character.isUpperCase(variable.name.charAt(0))) {
			error('Variable name should start with a lowercase', 
					ExpressionsDslPackage.Literals.VARIABLE__NAME,
					INVALID_NAME)
		}
	}
}
