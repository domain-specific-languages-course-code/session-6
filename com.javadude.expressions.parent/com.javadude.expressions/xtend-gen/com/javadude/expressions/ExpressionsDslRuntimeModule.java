/**
 * generated by Xtext 2.14.0
 */
package com.javadude.expressions;

import com.javadude.expressions.AbstractExpressionsDslRuntimeModule;

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
@SuppressWarnings("all")
public class ExpressionsDslRuntimeModule extends AbstractExpressionsDslRuntimeModule {
}
