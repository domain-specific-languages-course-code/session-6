package com.javadude.state.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import com.javadude.state.services.StateDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalStateDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'stateMachine'", "'{'", "'start'", "'}'", "'command'", "'code'", "'event'", "'resetEvent'", "'state'", "'action'", "'on'", "'switch'", "'to'", "'test'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalStateDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalStateDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalStateDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalStateDsl.g"; }



     	private StateDslGrammarAccess grammarAccess;

        public InternalStateDslParser(TokenStream input, StateDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "StateMachine";
       	}

       	@Override
       	protected StateDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleStateMachine"
    // InternalStateDsl.g:64:1: entryRuleStateMachine returns [EObject current=null] : iv_ruleStateMachine= ruleStateMachine EOF ;
    public final EObject entryRuleStateMachine() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStateMachine = null;


        try {
            // InternalStateDsl.g:64:53: (iv_ruleStateMachine= ruleStateMachine EOF )
            // InternalStateDsl.g:65:2: iv_ruleStateMachine= ruleStateMachine EOF
            {
             newCompositeNode(grammarAccess.getStateMachineRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStateMachine=ruleStateMachine();

            state._fsp--;

             current =iv_ruleStateMachine; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStateMachine"


    // $ANTLR start "ruleStateMachine"
    // InternalStateDsl.g:71:1: ruleStateMachine returns [EObject current=null] : (otherlv_0= 'stateMachine' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_commands_3_0= ruleCommand ) )* ( (lv_events_4_0= ruleEvent ) )* ( (lv_states_5_0= ruleState ) )+ ( (lv_resetEvents_6_0= ruleReset ) )* otherlv_7= 'start' ( (otherlv_8= RULE_ID ) ) ( (lv_tests_9_0= ruleTest ) )* otherlv_10= '}' ) ;
    public final EObject ruleStateMachine() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        EObject lv_commands_3_0 = null;

        EObject lv_events_4_0 = null;

        EObject lv_states_5_0 = null;

        EObject lv_resetEvents_6_0 = null;

        EObject lv_tests_9_0 = null;



        	enterRule();

        try {
            // InternalStateDsl.g:77:2: ( (otherlv_0= 'stateMachine' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_commands_3_0= ruleCommand ) )* ( (lv_events_4_0= ruleEvent ) )* ( (lv_states_5_0= ruleState ) )+ ( (lv_resetEvents_6_0= ruleReset ) )* otherlv_7= 'start' ( (otherlv_8= RULE_ID ) ) ( (lv_tests_9_0= ruleTest ) )* otherlv_10= '}' ) )
            // InternalStateDsl.g:78:2: (otherlv_0= 'stateMachine' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_commands_3_0= ruleCommand ) )* ( (lv_events_4_0= ruleEvent ) )* ( (lv_states_5_0= ruleState ) )+ ( (lv_resetEvents_6_0= ruleReset ) )* otherlv_7= 'start' ( (otherlv_8= RULE_ID ) ) ( (lv_tests_9_0= ruleTest ) )* otherlv_10= '}' )
            {
            // InternalStateDsl.g:78:2: (otherlv_0= 'stateMachine' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_commands_3_0= ruleCommand ) )* ( (lv_events_4_0= ruleEvent ) )* ( (lv_states_5_0= ruleState ) )+ ( (lv_resetEvents_6_0= ruleReset ) )* otherlv_7= 'start' ( (otherlv_8= RULE_ID ) ) ( (lv_tests_9_0= ruleTest ) )* otherlv_10= '}' )
            // InternalStateDsl.g:79:3: otherlv_0= 'stateMachine' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_commands_3_0= ruleCommand ) )* ( (lv_events_4_0= ruleEvent ) )* ( (lv_states_5_0= ruleState ) )+ ( (lv_resetEvents_6_0= ruleReset ) )* otherlv_7= 'start' ( (otherlv_8= RULE_ID ) ) ( (lv_tests_9_0= ruleTest ) )* otherlv_10= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getStateMachineAccess().getStateMachineKeyword_0());
            		
            // InternalStateDsl.g:83:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalStateDsl.g:84:4: (lv_name_1_0= RULE_ID )
            {
            // InternalStateDsl.g:84:4: (lv_name_1_0= RULE_ID )
            // InternalStateDsl.g:85:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getStateMachineAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getStateMachineRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getStateMachineAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalStateDsl.g:105:3: ( (lv_commands_3_0= ruleCommand ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==15) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalStateDsl.g:106:4: (lv_commands_3_0= ruleCommand )
            	    {
            	    // InternalStateDsl.g:106:4: (lv_commands_3_0= ruleCommand )
            	    // InternalStateDsl.g:107:5: lv_commands_3_0= ruleCommand
            	    {

            	    					newCompositeNode(grammarAccess.getStateMachineAccess().getCommandsCommandParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_5);
            	    lv_commands_3_0=ruleCommand();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStateMachineRule());
            	    					}
            	    					add(
            	    						current,
            	    						"commands",
            	    						lv_commands_3_0,
            	    						"com.javadude.state.StateDsl.Command");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalStateDsl.g:124:3: ( (lv_events_4_0= ruleEvent ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==17) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalStateDsl.g:125:4: (lv_events_4_0= ruleEvent )
            	    {
            	    // InternalStateDsl.g:125:4: (lv_events_4_0= ruleEvent )
            	    // InternalStateDsl.g:126:5: lv_events_4_0= ruleEvent
            	    {

            	    					newCompositeNode(grammarAccess.getStateMachineAccess().getEventsEventParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_5);
            	    lv_events_4_0=ruleEvent();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStateMachineRule());
            	    					}
            	    					add(
            	    						current,
            	    						"events",
            	    						lv_events_4_0,
            	    						"com.javadude.state.StateDsl.Event");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // InternalStateDsl.g:143:3: ( (lv_states_5_0= ruleState ) )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==19) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalStateDsl.g:144:4: (lv_states_5_0= ruleState )
            	    {
            	    // InternalStateDsl.g:144:4: (lv_states_5_0= ruleState )
            	    // InternalStateDsl.g:145:5: lv_states_5_0= ruleState
            	    {

            	    					newCompositeNode(grammarAccess.getStateMachineAccess().getStatesStateParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_6);
            	    lv_states_5_0=ruleState();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStateMachineRule());
            	    					}
            	    					add(
            	    						current,
            	    						"states",
            	    						lv_states_5_0,
            	    						"com.javadude.state.StateDsl.State");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);

            // InternalStateDsl.g:162:3: ( (lv_resetEvents_6_0= ruleReset ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==18) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalStateDsl.g:163:4: (lv_resetEvents_6_0= ruleReset )
            	    {
            	    // InternalStateDsl.g:163:4: (lv_resetEvents_6_0= ruleReset )
            	    // InternalStateDsl.g:164:5: lv_resetEvents_6_0= ruleReset
            	    {

            	    					newCompositeNode(grammarAccess.getStateMachineAccess().getResetEventsResetParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_7);
            	    lv_resetEvents_6_0=ruleReset();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStateMachineRule());
            	    					}
            	    					add(
            	    						current,
            	    						"resetEvents",
            	    						lv_resetEvents_6_0,
            	    						"com.javadude.state.StateDsl.Reset");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_7=(Token)match(input,13,FOLLOW_3); 

            			newLeafNode(otherlv_7, grammarAccess.getStateMachineAccess().getStartKeyword_7());
            		
            // InternalStateDsl.g:185:3: ( (otherlv_8= RULE_ID ) )
            // InternalStateDsl.g:186:4: (otherlv_8= RULE_ID )
            {
            // InternalStateDsl.g:186:4: (otherlv_8= RULE_ID )
            // InternalStateDsl.g:187:5: otherlv_8= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getStateMachineRule());
            					}
            				
            otherlv_8=(Token)match(input,RULE_ID,FOLLOW_8); 

            					newLeafNode(otherlv_8, grammarAccess.getStateMachineAccess().getStartStateStateCrossReference_8_0());
            				

            }


            }

            // InternalStateDsl.g:198:3: ( (lv_tests_9_0= ruleTest ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==24) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalStateDsl.g:199:4: (lv_tests_9_0= ruleTest )
            	    {
            	    // InternalStateDsl.g:199:4: (lv_tests_9_0= ruleTest )
            	    // InternalStateDsl.g:200:5: lv_tests_9_0= ruleTest
            	    {

            	    					newCompositeNode(grammarAccess.getStateMachineAccess().getTestsTestParserRuleCall_9_0());
            	    				
            	    pushFollow(FOLLOW_8);
            	    lv_tests_9_0=ruleTest();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStateMachineRule());
            	    					}
            	    					add(
            	    						current,
            	    						"tests",
            	    						lv_tests_9_0,
            	    						"com.javadude.state.StateDsl.Test");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            otherlv_10=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getStateMachineAccess().getRightCurlyBracketKeyword_10());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStateMachine"


    // $ANTLR start "entryRuleCommand"
    // InternalStateDsl.g:225:1: entryRuleCommand returns [EObject current=null] : iv_ruleCommand= ruleCommand EOF ;
    public final EObject entryRuleCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCommand = null;


        try {
            // InternalStateDsl.g:225:48: (iv_ruleCommand= ruleCommand EOF )
            // InternalStateDsl.g:226:2: iv_ruleCommand= ruleCommand EOF
            {
             newCompositeNode(grammarAccess.getCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCommand=ruleCommand();

            state._fsp--;

             current =iv_ruleCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCommand"


    // $ANTLR start "ruleCommand"
    // InternalStateDsl.g:232:1: ruleCommand returns [EObject current=null] : (otherlv_0= 'command' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'code' ( (lv_code_3_0= RULE_ID ) ) ) ;
    public final EObject ruleCommand() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_code_3_0=null;


        	enterRule();

        try {
            // InternalStateDsl.g:238:2: ( (otherlv_0= 'command' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'code' ( (lv_code_3_0= RULE_ID ) ) ) )
            // InternalStateDsl.g:239:2: (otherlv_0= 'command' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'code' ( (lv_code_3_0= RULE_ID ) ) )
            {
            // InternalStateDsl.g:239:2: (otherlv_0= 'command' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'code' ( (lv_code_3_0= RULE_ID ) ) )
            // InternalStateDsl.g:240:3: otherlv_0= 'command' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'code' ( (lv_code_3_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,15,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getCommandAccess().getCommandKeyword_0());
            		
            // InternalStateDsl.g:244:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalStateDsl.g:245:4: (lv_name_1_0= RULE_ID )
            {
            // InternalStateDsl.g:245:4: (lv_name_1_0= RULE_ID )
            // InternalStateDsl.g:246:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(lv_name_1_0, grammarAccess.getCommandAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCommandRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getCommandAccess().getCodeKeyword_2());
            		
            // InternalStateDsl.g:266:3: ( (lv_code_3_0= RULE_ID ) )
            // InternalStateDsl.g:267:4: (lv_code_3_0= RULE_ID )
            {
            // InternalStateDsl.g:267:4: (lv_code_3_0= RULE_ID )
            // InternalStateDsl.g:268:5: lv_code_3_0= RULE_ID
            {
            lv_code_3_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_code_3_0, grammarAccess.getCommandAccess().getCodeIDTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCommandRule());
            					}
            					setWithLastConsumed(
            						current,
            						"code",
            						lv_code_3_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCommand"


    // $ANTLR start "entryRuleEvent"
    // InternalStateDsl.g:288:1: entryRuleEvent returns [EObject current=null] : iv_ruleEvent= ruleEvent EOF ;
    public final EObject entryRuleEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEvent = null;


        try {
            // InternalStateDsl.g:288:46: (iv_ruleEvent= ruleEvent EOF )
            // InternalStateDsl.g:289:2: iv_ruleEvent= ruleEvent EOF
            {
             newCompositeNode(grammarAccess.getEventRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEvent=ruleEvent();

            state._fsp--;

             current =iv_ruleEvent; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEvent"


    // $ANTLR start "ruleEvent"
    // InternalStateDsl.g:295:1: ruleEvent returns [EObject current=null] : (otherlv_0= 'event' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'code' ( (lv_code_3_0= RULE_ID ) ) ) ;
    public final EObject ruleEvent() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_code_3_0=null;


        	enterRule();

        try {
            // InternalStateDsl.g:301:2: ( (otherlv_0= 'event' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'code' ( (lv_code_3_0= RULE_ID ) ) ) )
            // InternalStateDsl.g:302:2: (otherlv_0= 'event' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'code' ( (lv_code_3_0= RULE_ID ) ) )
            {
            // InternalStateDsl.g:302:2: (otherlv_0= 'event' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'code' ( (lv_code_3_0= RULE_ID ) ) )
            // InternalStateDsl.g:303:3: otherlv_0= 'event' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'code' ( (lv_code_3_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,17,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getEventAccess().getEventKeyword_0());
            		
            // InternalStateDsl.g:307:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalStateDsl.g:308:4: (lv_name_1_0= RULE_ID )
            {
            // InternalStateDsl.g:308:4: (lv_name_1_0= RULE_ID )
            // InternalStateDsl.g:309:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(lv_name_1_0, grammarAccess.getEventAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEventRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getEventAccess().getCodeKeyword_2());
            		
            // InternalStateDsl.g:329:3: ( (lv_code_3_0= RULE_ID ) )
            // InternalStateDsl.g:330:4: (lv_code_3_0= RULE_ID )
            {
            // InternalStateDsl.g:330:4: (lv_code_3_0= RULE_ID )
            // InternalStateDsl.g:331:5: lv_code_3_0= RULE_ID
            {
            lv_code_3_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_code_3_0, grammarAccess.getEventAccess().getCodeIDTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEventRule());
            					}
            					setWithLastConsumed(
            						current,
            						"code",
            						lv_code_3_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEvent"


    // $ANTLR start "entryRuleReset"
    // InternalStateDsl.g:351:1: entryRuleReset returns [EObject current=null] : iv_ruleReset= ruleReset EOF ;
    public final EObject entryRuleReset() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReset = null;


        try {
            // InternalStateDsl.g:351:46: (iv_ruleReset= ruleReset EOF )
            // InternalStateDsl.g:352:2: iv_ruleReset= ruleReset EOF
            {
             newCompositeNode(grammarAccess.getResetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleReset=ruleReset();

            state._fsp--;

             current =iv_ruleReset; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReset"


    // $ANTLR start "ruleReset"
    // InternalStateDsl.g:358:1: ruleReset returns [EObject current=null] : (otherlv_0= 'resetEvent' ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleReset() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalStateDsl.g:364:2: ( (otherlv_0= 'resetEvent' ( (otherlv_1= RULE_ID ) ) ) )
            // InternalStateDsl.g:365:2: (otherlv_0= 'resetEvent' ( (otherlv_1= RULE_ID ) ) )
            {
            // InternalStateDsl.g:365:2: (otherlv_0= 'resetEvent' ( (otherlv_1= RULE_ID ) ) )
            // InternalStateDsl.g:366:3: otherlv_0= 'resetEvent' ( (otherlv_1= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,18,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getResetAccess().getResetEventKeyword_0());
            		
            // InternalStateDsl.g:370:3: ( (otherlv_1= RULE_ID ) )
            // InternalStateDsl.g:371:4: (otherlv_1= RULE_ID )
            {
            // InternalStateDsl.g:371:4: (otherlv_1= RULE_ID )
            // InternalStateDsl.g:372:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getResetRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_1, grammarAccess.getResetAccess().getEventEventCrossReference_1_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReset"


    // $ANTLR start "entryRuleState"
    // InternalStateDsl.g:387:1: entryRuleState returns [EObject current=null] : iv_ruleState= ruleState EOF ;
    public final EObject entryRuleState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleState = null;


        try {
            // InternalStateDsl.g:387:46: (iv_ruleState= ruleState EOF )
            // InternalStateDsl.g:388:2: iv_ruleState= ruleState EOF
            {
             newCompositeNode(grammarAccess.getStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleState=ruleState();

            state._fsp--;

             current =iv_ruleState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // InternalStateDsl.g:394:1: ruleState returns [EObject current=null] : (otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_actions_3_0= ruleAction ) )* ( (lv_transitions_4_0= ruleTransition ) )* otherlv_5= '}' ) ;
    public final EObject ruleState() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        EObject lv_actions_3_0 = null;

        EObject lv_transitions_4_0 = null;



        	enterRule();

        try {
            // InternalStateDsl.g:400:2: ( (otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_actions_3_0= ruleAction ) )* ( (lv_transitions_4_0= ruleTransition ) )* otherlv_5= '}' ) )
            // InternalStateDsl.g:401:2: (otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_actions_3_0= ruleAction ) )* ( (lv_transitions_4_0= ruleTransition ) )* otherlv_5= '}' )
            {
            // InternalStateDsl.g:401:2: (otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_actions_3_0= ruleAction ) )* ( (lv_transitions_4_0= ruleTransition ) )* otherlv_5= '}' )
            // InternalStateDsl.g:402:3: otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_actions_3_0= ruleAction ) )* ( (lv_transitions_4_0= ruleTransition ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,19,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getStateAccess().getStateKeyword_0());
            		
            // InternalStateDsl.g:406:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalStateDsl.g:407:4: (lv_name_1_0= RULE_ID )
            {
            // InternalStateDsl.g:407:4: (lv_name_1_0= RULE_ID )
            // InternalStateDsl.g:408:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getStateAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getStateRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_10); 

            			newLeafNode(otherlv_2, grammarAccess.getStateAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalStateDsl.g:428:3: ( (lv_actions_3_0= ruleAction ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==20) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalStateDsl.g:429:4: (lv_actions_3_0= ruleAction )
            	    {
            	    // InternalStateDsl.g:429:4: (lv_actions_3_0= ruleAction )
            	    // InternalStateDsl.g:430:5: lv_actions_3_0= ruleAction
            	    {

            	    					newCompositeNode(grammarAccess.getStateAccess().getActionsActionParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_10);
            	    lv_actions_3_0=ruleAction();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStateRule());
            	    					}
            	    					add(
            	    						current,
            	    						"actions",
            	    						lv_actions_3_0,
            	    						"com.javadude.state.StateDsl.Action");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            // InternalStateDsl.g:447:3: ( (lv_transitions_4_0= ruleTransition ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==21) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalStateDsl.g:448:4: (lv_transitions_4_0= ruleTransition )
            	    {
            	    // InternalStateDsl.g:448:4: (lv_transitions_4_0= ruleTransition )
            	    // InternalStateDsl.g:449:5: lv_transitions_4_0= ruleTransition
            	    {

            	    					newCompositeNode(grammarAccess.getStateAccess().getTransitionsTransitionParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_11);
            	    lv_transitions_4_0=ruleTransition();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStateRule());
            	    					}
            	    					add(
            	    						current,
            	    						"transitions",
            	    						lv_transitions_4_0,
            	    						"com.javadude.state.StateDsl.Transition");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            otherlv_5=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getStateAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRuleAction"
    // InternalStateDsl.g:474:1: entryRuleAction returns [EObject current=null] : iv_ruleAction= ruleAction EOF ;
    public final EObject entryRuleAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAction = null;


        try {
            // InternalStateDsl.g:474:47: (iv_ruleAction= ruleAction EOF )
            // InternalStateDsl.g:475:2: iv_ruleAction= ruleAction EOF
            {
             newCompositeNode(grammarAccess.getActionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAction=ruleAction();

            state._fsp--;

             current =iv_ruleAction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalStateDsl.g:481:1: ruleAction returns [EObject current=null] : (otherlv_0= 'action' ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleAction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalStateDsl.g:487:2: ( (otherlv_0= 'action' ( (otherlv_1= RULE_ID ) ) ) )
            // InternalStateDsl.g:488:2: (otherlv_0= 'action' ( (otherlv_1= RULE_ID ) ) )
            {
            // InternalStateDsl.g:488:2: (otherlv_0= 'action' ( (otherlv_1= RULE_ID ) ) )
            // InternalStateDsl.g:489:3: otherlv_0= 'action' ( (otherlv_1= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,20,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getActionAccess().getActionKeyword_0());
            		
            // InternalStateDsl.g:493:3: ( (otherlv_1= RULE_ID ) )
            // InternalStateDsl.g:494:4: (otherlv_1= RULE_ID )
            {
            // InternalStateDsl.g:494:4: (otherlv_1= RULE_ID )
            // InternalStateDsl.g:495:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getActionRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_1, grammarAccess.getActionAccess().getCommandCommandCrossReference_1_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleTransition"
    // InternalStateDsl.g:510:1: entryRuleTransition returns [EObject current=null] : iv_ruleTransition= ruleTransition EOF ;
    public final EObject entryRuleTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransition = null;


        try {
            // InternalStateDsl.g:510:51: (iv_ruleTransition= ruleTransition EOF )
            // InternalStateDsl.g:511:2: iv_ruleTransition= ruleTransition EOF
            {
             newCompositeNode(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTransition=ruleTransition();

            state._fsp--;

             current =iv_ruleTransition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalStateDsl.g:517:1: ruleTransition returns [EObject current=null] : (otherlv_0= 'on' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'switch' otherlv_3= 'to' ( (otherlv_4= RULE_ID ) ) ) ;
    public final EObject ruleTransition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;


        	enterRule();

        try {
            // InternalStateDsl.g:523:2: ( (otherlv_0= 'on' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'switch' otherlv_3= 'to' ( (otherlv_4= RULE_ID ) ) ) )
            // InternalStateDsl.g:524:2: (otherlv_0= 'on' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'switch' otherlv_3= 'to' ( (otherlv_4= RULE_ID ) ) )
            {
            // InternalStateDsl.g:524:2: (otherlv_0= 'on' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'switch' otherlv_3= 'to' ( (otherlv_4= RULE_ID ) ) )
            // InternalStateDsl.g:525:3: otherlv_0= 'on' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'switch' otherlv_3= 'to' ( (otherlv_4= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,21,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getTransitionAccess().getOnKeyword_0());
            		
            // InternalStateDsl.g:529:3: ( (otherlv_1= RULE_ID ) )
            // InternalStateDsl.g:530:4: (otherlv_1= RULE_ID )
            {
            // InternalStateDsl.g:530:4: (otherlv_1= RULE_ID )
            // InternalStateDsl.g:531:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_12); 

            					newLeafNode(otherlv_1, grammarAccess.getTransitionAccess().getEventEventCrossReference_1_0());
            				

            }


            }

            otherlv_2=(Token)match(input,22,FOLLOW_13); 

            			newLeafNode(otherlv_2, grammarAccess.getTransitionAccess().getSwitchKeyword_2());
            		
            otherlv_3=(Token)match(input,23,FOLLOW_3); 

            			newLeafNode(otherlv_3, grammarAccess.getTransitionAccess().getToKeyword_3());
            		
            // InternalStateDsl.g:550:3: ( (otherlv_4= RULE_ID ) )
            // InternalStateDsl.g:551:4: (otherlv_4= RULE_ID )
            {
            // InternalStateDsl.g:551:4: (otherlv_4= RULE_ID )
            // InternalStateDsl.g:552:5: otherlv_4= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            				
            otherlv_4=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_4, grammarAccess.getTransitionAccess().getTargetStateCrossReference_4_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "entryRuleTest"
    // InternalStateDsl.g:567:1: entryRuleTest returns [EObject current=null] : iv_ruleTest= ruleTest EOF ;
    public final EObject entryRuleTest() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTest = null;


        try {
            // InternalStateDsl.g:567:45: (iv_ruleTest= ruleTest EOF )
            // InternalStateDsl.g:568:2: iv_ruleTest= ruleTest EOF
            {
             newCompositeNode(grammarAccess.getTestRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTest=ruleTest();

            state._fsp--;

             current =iv_ruleTest; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTest"


    // $ANTLR start "ruleTest"
    // InternalStateDsl.g:574:1: ruleTest returns [EObject current=null] : (otherlv_0= 'test' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (otherlv_3= RULE_ID ) )+ otherlv_4= '}' ) ;
    public final EObject ruleTest() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;


        	enterRule();

        try {
            // InternalStateDsl.g:580:2: ( (otherlv_0= 'test' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (otherlv_3= RULE_ID ) )+ otherlv_4= '}' ) )
            // InternalStateDsl.g:581:2: (otherlv_0= 'test' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (otherlv_3= RULE_ID ) )+ otherlv_4= '}' )
            {
            // InternalStateDsl.g:581:2: (otherlv_0= 'test' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (otherlv_3= RULE_ID ) )+ otherlv_4= '}' )
            // InternalStateDsl.g:582:3: otherlv_0= 'test' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (otherlv_3= RULE_ID ) )+ otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,24,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getTestAccess().getTestKeyword_0());
            		
            // InternalStateDsl.g:586:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalStateDsl.g:587:4: (lv_name_1_0= RULE_ID )
            {
            // InternalStateDsl.g:587:4: (lv_name_1_0= RULE_ID )
            // InternalStateDsl.g:588:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getTestAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTestRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getTestAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalStateDsl.g:608:3: ( (otherlv_3= RULE_ID ) )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==RULE_ID) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalStateDsl.g:609:4: (otherlv_3= RULE_ID )
            	    {
            	    // InternalStateDsl.g:609:4: (otherlv_3= RULE_ID )
            	    // InternalStateDsl.g:610:5: otherlv_3= RULE_ID
            	    {

            	    					if (current==null) {
            	    						current = createModelElement(grammarAccess.getTestRule());
            	    					}
            	    				
            	    otherlv_3=(Token)match(input,RULE_ID,FOLLOW_14); 

            	    					newLeafNode(otherlv_3, grammarAccess.getTestAccess().getEventsEventCrossReference_3_0());
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);

            otherlv_4=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getTestAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTest"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x00000000000A8000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x00000000000EA000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000042000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000001004000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000304000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000204000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000004010L});

}