package test1

import com.javadude.state.StateDslStandaloneSetup
import com.javadude.state.stateDsl.Command
import com.javadude.state.stateDsl.Event
import com.javadude.state.stateDsl.State
import com.javadude.state.stateDsl.StateMachine
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet

class CommandChannel2 {
    fun send(action: Command) {
        println("    Action: ${action.name} / ${action.code}")
    }
}

fun getNextState(stateMachine : StateMachine,
                 state : State,
                 event : Event) : State? {
    state.transitions.forEach {
        if (it.event == event) {
            return it.target
        }
    }
    stateMachine.resetEvents.forEach {
        if (it.event == event) {
            return stateMachine.startState
        }
    }
    return null
}

class Controller2(private val stateMachine : StateMachine,
                  private val commandChannel : CommandChannel2) {
    private var currentState = stateMachine.startState

    fun handle(event : Event) {
        println("Event: ${event.name}")

        getNextState(stateMachine, currentState, event)?.let {
            currentState = it
            println("  changed state to ${currentState.name}")
            currentState.actions.forEach { commandChannel.send(it.command) }
        }
    }
}

class Test2 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val injector = StateDslStandaloneSetup().createInjectorAndDoEMFRegistration()
            val rs = injector.getInstance(ResourceSet::class.java)
            val resource = rs.getResource(URI.createURI("test1/GothicSecurity.states"), true)
            resource.load(null)
            val stateMachine = resource.contents[0] as com.javadude.state.stateDsl.StateMachine

            stateMachine.tests.forEach { test ->
                println()
                println("Running test ${test.name}")
                val c = Controller2(stateMachine, CommandChannel2())
                test.events.forEach { event ->
                    c.handle(event)
                }
            }


// VALIDATION - NOT CONVERTED TO KOTLIN OR TESTED
//	        IResourceValidator validator = injector.getInstance(IResourceValidator.class);
//	        List<Issue> issues = validator.validate(resource,
//	                CheckMode.ALL, CancelIndicator.NullImpl);
//	        for (Issue issue: issues) {
//	            switch (issue.getSeverity()) {
//	                case ERROR:
//	                    System.out.println("ERROR: " + issue.getMessage());
//	                case WARNING:
//	                        System.out.println("WARNING: " + issue.getMessage());
//	            }
//	        }
        }
    }
}