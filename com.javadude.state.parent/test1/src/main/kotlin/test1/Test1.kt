package test1

import com.javadude.state.StateDslStandaloneSetup
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import java.lang.IllegalArgumentException

class Command(val name : String,
              val code : String)

class Event(val name : String,
            val code : String)

class State(val name : String) {
    lateinit var actions : List<Command>
    lateinit var transitions: Map<Event, State>
}

class StateMachine(val startState : State) {
    lateinit var resetEvents : Set<Event>

    fun resetTarget(event : Event) =
            if (resetEvents.contains(event))
                startState
            else null
}

class CommandChannel {
    fun send(action: Command) {
        println("    Action: ${action.name} / ${action.code}")
    }
}

class Controller(private val stateMachine : StateMachine,
                 private val commandChannel : CommandChannel) {
    private var currentState = stateMachine.startState

    fun handle(event : Event) {
        println("Event: ${event.name}")

        val nextState =
                currentState.transitions[event] ?:
                stateMachine.resetTarget(event)
        nextState?.let {
            currentState = it
            println("  changed state to ${currentState.name}")
            currentState.actions.forEach { commandChannel.send(it) }
        }
    }
}

class Test1 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val injector = StateDslStandaloneSetup().createInjectorAndDoEMFRegistration()
            val rs = injector.getInstance(ResourceSet::class.java)
            val resource = rs.getResource(URI.createURI("test1/GothicSecurity.states"), true)
            resource.load(null)
            val stateMachineDef = resource.contents[0] as com.javadude.state.stateDsl.StateMachine

            val commands = stateMachineDef.commands.map {
                it.name to Command(it.name, it.code)
            }.toMap()

            val events = stateMachineDef.events.map {
                it.name to Event(it.name, it.code)
            }.toMap()

            val states = stateMachineDef.states.map {
                it.name to State(it.name)
            }.toMap()

            stateMachineDef.states.forEach {
                states[it.name]!!.actions = it.actions.mapNotNull {action-> commands[action.command.name] }
                states[it.name]!!.transitions = it.transitions.mapNotNull {
                    transition->
                        events[transition.event.name]?.let {event->
                            states[transition.target.name]?.let { state ->
                                event to state
                            } ?: throw IllegalArgumentException("state ${transition.target.name} not defined")
                        } ?: throw IllegalArgumentException("event ${transition.event.name} not defined")
                }.toMap()
            }

            val stateMachine = StateMachine(
                    states[stateMachineDef.startState.name] ?: throw IllegalArgumentException("..."))

            stateMachine.resetEvents = stateMachineDef.resetEvents.mapNotNull {
                events[it.event.name]
            }.toSet()

            stateMachineDef.tests.forEach { test ->
                println()
                println("Running test ${test.name}")
                val c = Controller(stateMachine, CommandChannel())
                test.events.forEach { event ->
                    c.handle(events[event.name] ?: throw IllegalArgumentException("..."))
                }
            }


// VALIDATION - NOT CONVERTED TO KOTLIN OR TESTED
//	        IResourceValidator validator = injector.getInstance(IResourceValidator.class);
//	        List<Issue> issues = validator.validate(resource,
//	                CheckMode.ALL, CancelIndicator.NullImpl);
//	        for (Issue issue: issues) {
//	            switch (issue.getSeverity()) {
//	                case ERROR:
//	                    System.out.println("ERROR: " + issue.getMessage());
//	                case WARNING:
//	                        System.out.println("WARNING: " + issue.getMessage());
//	            }
//	        }
        }
    }
}